﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class JUGADOR
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
        }

        private List<enumJuegos> jugadas;

        public List<enumJuegos> Jugadas
        {
            get { return jugadas; }
        }
    }
}