﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class JUEGO
    {
        public JUEGO(enumJuegos tip, int puntos)
        {
            tipo = tip;
            puntaje = puntos;

        }

        private enumJuegos tipo;

        public enumJuegos Tipo
        {
            get { return tipo; }
        }

        private int puntaje;

        public int Puntaje
        {
            get { return puntaje; }
        }
    }
}
