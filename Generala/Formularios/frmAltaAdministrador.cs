﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class frmAltaAdministrador : Form
    {
        ADMINISTRADOR administrador;

        public frmAltaAdministrador()
        {
            InitializeComponent();
        }

        private void btnCrearAdmin_Click(object sender, EventArgs e)
        {
            if (comboUsuarios.SelectedItem != null)
            {
                administrador.BajaUsuario(((USUARIO)comboUsuarios.SelectedValue).Username);
                ListarUsuarios();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un usuario para continuar.");
            }
        }

        private void frmAltaAdministrador_Load(object sender, EventArgs e)
        {
            ListarUsuarios();
        }
        public void ObtenerAdmin(ADMINISTRADOR admin)
        {
            administrador = admin;
        }

        void ListarUsuarios()
        {
            comboUsuarios.DataSource = null;
            comboUsuarios.DataSource = ADMINISTRADOR.listarUsuarios(true);
        }

    }
}
