﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class frmDesbloquearUsuario : Form
    {
        ADMINISTRADOR administrador;

        public frmDesbloquearUsuario()
        {
            InitializeComponent();
        }

        public void ObtenerAdmin(ADMINISTRADOR admin)
        {
            administrador = admin;
        }

        void ListarUsuarios()
        {
            comboUsuariosBloqueados.DataSource = null;
            comboUsuariosBloqueados.DataSource = ADMINISTRADOR.listarUsuarios(true);
        }

        private void frmDesbloquearUsuario_Load(object sender, EventArgs e)
        {
            ListarUsuarios();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboUsuariosBloqueados.SelectedItem != null)
            {
                administrador.DesbloquearUsuario(((USUARIO)comboUsuariosBloqueados.SelectedValue).Username);
                ListarUsuarios();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un usuario bloqueado para continuar.");
            }
        }
    }
}
