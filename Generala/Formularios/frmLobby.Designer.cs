﻿namespace Generala
{
    partial class frmLobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLobby));
            this.btnIniciarPartida = new System.Windows.Forms.Button();
            this.txtJugador1 = new System.Windows.Forms.TextBox();
            this.txtJugador2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbPanelAdmin = new System.Windows.Forms.GroupBox();
            this.btnNuevoAdmin = new System.Windows.Forms.Button();
            this.btnEliminarUsuario = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.gbPanelAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnIniciarPartida
            // 
            this.btnIniciarPartida.Location = new System.Drawing.Point(11, 30);
            this.btnIniciarPartida.Margin = new System.Windows.Forms.Padding(2);
            this.btnIniciarPartida.Name = "btnIniciarPartida";
            this.btnIniciarPartida.Size = new System.Drawing.Size(72, 50);
            this.btnIniciarPartida.TabIndex = 0;
            this.btnIniciarPartida.Text = "Iniciar partida";
            this.btnIniciarPartida.UseVisualStyleBackColor = true;
            this.btnIniciarPartida.Click += new System.EventHandler(this.btnIniciarPartida_Click);
            // 
            // txtJugador1
            // 
            this.txtJugador1.Location = new System.Drawing.Point(147, 36);
            this.txtJugador1.Margin = new System.Windows.Forms.Padding(2);
            this.txtJugador1.Name = "txtJugador1";
            this.txtJugador1.Size = new System.Drawing.Size(76, 20);
            this.txtJugador1.TabIndex = 1;
            // 
            // txtJugador2
            // 
            this.txtJugador2.Location = new System.Drawing.Point(147, 59);
            this.txtJugador2.Margin = new System.Windows.Forms.Padding(2);
            this.txtJugador2.Name = "txtJugador2";
            this.txtJugador2.Size = new System.Drawing.Size(76, 20);
            this.txtJugador2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(87, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Jugador 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Jugador 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "label3";
            // 
            // gbPanelAdmin
            // 
            this.gbPanelAdmin.Controls.Add(this.btnNuevoAdmin);
            this.gbPanelAdmin.Controls.Add(this.btnEliminarUsuario);
            this.gbPanelAdmin.Controls.Add(this.button1);
            this.gbPanelAdmin.Location = new System.Drawing.Point(282, 30);
            this.gbPanelAdmin.Margin = new System.Windows.Forms.Padding(2);
            this.gbPanelAdmin.Name = "gbPanelAdmin";
            this.gbPanelAdmin.Padding = new System.Windows.Forms.Padding(2);
            this.gbPanelAdmin.Size = new System.Drawing.Size(146, 110);
            this.gbPanelAdmin.TabIndex = 6;
            this.gbPanelAdmin.TabStop = false;
            this.gbPanelAdmin.Text = "Panel de administrador";
            this.gbPanelAdmin.Visible = false;
            // 
            // btnNuevoAdmin
            // 
            this.btnNuevoAdmin.Location = new System.Drawing.Point(5, 80);
            this.btnNuevoAdmin.Margin = new System.Windows.Forms.Padding(2);
            this.btnNuevoAdmin.Name = "btnNuevoAdmin";
            this.btnNuevoAdmin.Size = new System.Drawing.Size(135, 26);
            this.btnNuevoAdmin.TabIndex = 2;
            this.btnNuevoAdmin.Text = "Nuevo administrador";
            this.btnNuevoAdmin.UseVisualStyleBackColor = true;
            this.btnNuevoAdmin.Click += new System.EventHandler(this.btnNuevoAdmin_Click);
            // 
            // btnEliminarUsuario
            // 
            this.btnEliminarUsuario.Location = new System.Drawing.Point(5, 49);
            this.btnEliminarUsuario.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminarUsuario.Name = "btnEliminarUsuario";
            this.btnEliminarUsuario.Size = new System.Drawing.Size(135, 26);
            this.btnEliminarUsuario.TabIndex = 1;
            this.btnEliminarUsuario.Text = "Eliminar usuario";
            this.btnEliminarUsuario.UseVisualStyleBackColor = true;
            this.btnEliminarUsuario.Click += new System.EventHandler(this.btnEliminarUsuario_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 18);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 26);
            this.button1.TabIndex = 0;
            this.button1.Text = "Desbloquear usuario";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.Location = new System.Drawing.Point(314, 7);
            this.btnCerrarSesion.Margin = new System.Windows.Forms.Padding(2);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(114, 19);
            this.btnCerrarSesion.TabIndex = 7;
            this.btnCerrarSesion.Text = "Cerrar sesion";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // frmLobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(450, 162);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.gbPanelAdmin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtJugador2);
            this.Controls.Add(this.txtJugador1);
            this.Controls.Add(this.btnIniciarPartida);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmLobby";
            this.Text = "frmLobby";
            this.Load += new System.EventHandler(this.frmLobby_Load);
            this.gbPanelAdmin.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIniciarPartida;
        private System.Windows.Forms.TextBox txtJugador1;
        private System.Windows.Forms.TextBox txtJugador2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbPanelAdmin;
        private System.Windows.Forms.Button btnNuevoAdmin;
        private System.Windows.Forms.Button btnEliminarUsuario;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnCerrarSesion;
    }
}