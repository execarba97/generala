﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class Juego : Form
    {
        TABLERO tablero = new TABLERO();
        List<Control> controlDado = new List<Control>();
        List<Control> jugadaActual = new List<Control>();
        List<DADO> dados = new List<DADO>();
        List<controlJugada> controles_jugadas = new List<controlJugada>();
        Anotador anotador = new Anotador();
        JUGADOR j1 = new JUGADOR();
        JUGADOR j2 = new JUGADOR();
        JUGADOR jugadorActual;

        int control = 0, turnos = 1;
        bool turno = true;

        public Juego()
        {
            InitializeComponent();
            jugadorActual = j1;

            //anotador.ConfigurarJugadores(j1, j2);
        }

        private void Juego_Load(object sender, EventArgs e)
        {
            tablero.TirarDados += Tablero_TirarDados;
            tablero.Tirar(5);
            CalcularJugada();
            this.Controls.Add(anotador);
            anotador.Location = new Point(624, 36);
            anotador.configurarContadores(j1, j2);
            label1.Text = "Es el turno del jugador: " + jugadorActual.Nombre;
        }

        private void btnTirar_Click(object sender, EventArgs e)
        {
            //JugadasDisponibles(jugadorActual);
            TirarDados();
        }

        private void btnSeleccionarJugada_Click(object sender, EventArgs e)
        {
            Anotar();
        }

        private void btnAnularJugada_Click(object sender, EventArgs e)
        {
            Anotar(true);
        }
        private static List<T> GetEnumList<T>()
        {
            T[] array = (T[])Enum.GetValues(typeof(T));
            List<T> list = new List<T>(array);
            return list;
        }

        void JugadasDisponibles(JUGADOR jugador)
        {
            List<enumJuegos> listaDiferente = GetEnumList<enumJuegos>().Except(jugadorActual.Jugadas).ToList();

            int x = 470, y = 36;
            foreach (enumJuegos j in listaDiferente)
            {
                if(j != enumJuegos.total)
                {
                    controlJugada l = new controlJugada();
                    l.Location = new Point(x, y);
                    l.Nombre = j;
                    //MessageBox.Show(j.ToString());
                    this.controles_jugadas.Add(l);
                    this.Controls.Add(l);
                    y += 25;
                }
            }
        }

        void Anotar(bool anulado = false)
        {
            /*
            if(CantidadSeleccionada())
            {
                controlJugada c = (from controlJugada control in controles_jugadas
                                   where control.Seleccionado = true
                                   select control).First();

                int puntaje = 0;

                if (!anulado)
                {
                    puntaje = JUEGO.CalcularPuntos(c.Nombre, control, dados);
                }

                anotador.IngresarValor(c.Nombre, puntaje, jugadorActual);
                jugadorActual.AnotarJugada(c.Nombre, puntaje);
                turno = false;
                turnos += 1;

                anotador.IngresarValor(enumJuegos.total, jugadorActual.Puntos, jugadorActual);

                btnSeleccionarJugada.Visible = true;
                btnAnularJugada.Visible = false;
                TirarDados();
            }
            */
            
            bool encontrado = false;
            foreach (controlJugada c in controles_jugadas)
            {
                if (c.Seleccionado && !encontrado)
                {
                    int puntaje = 0;

                    if (!anulado)
                    {
                        puntaje = JUEGO.CalcularPuntos(c.Nombre, control, dados);
                    }

                    anotador.IngresarValor(c.Nombre, puntaje, jugadorActual);
                    jugadorActual.AnotarJugada(c.Nombre, puntaje);
                    turno = false;
                    encontrado = true;
                    turnos += 1;
                }
            }

            anotador.IngresarValor(enumJuegos.total, jugadorActual.Puntos, jugadorActual);

            if (encontrado)
            {
                btnSeleccionarJugada.Visible = true;
                btnAnularJugada.Visible = false;
                TirarDados();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una jugada antes de continuar.");
            }
            
        }

        void TirarDados()
        {
            if(turnos == 23 || jugadorActual.Puntos > 10000)
            {
                MessageBox.Show("Juego terminado.");
            }
            else
            {
                int cantidad = 0;
                if (!turno)
                {
                    control = 0;
                    LimpiarTablero(controlDado);
                    LimpiarTablero(jugadaActual);
                    controlDado.Clear();
                    dados.Clear();
                    jugadaActual.Clear();
                    cantidad = 5;
                    CambiarDeJugador();
                    turno = true;

                    rutina();
                }
                else if (control == 2 && turno)
                {
                    MessageBox.Show("Debe seleccionar una jugada antes de continuar.");
                }
                else
                {
                    control += 1;
                    cantidad = LimpiarTableroParcial();
                    rutina();
                }

                void rutina()
                {
                    LimpiarControlJugada();
                    tablero.Tirar(cantidad);
                    CalcularJugada();
                }
            }
        }

        void CalcularJugada()
        {
            int x = 423, y = 41;
            foreach (enumJuegos j in tablero.CalcularJugada(dados))
            {
                if(!buscarJugada(j))
                {
                    controlJugada l = new controlJugada();
                    l.Location = new Point(x, y);
                    l.Nombre = j;
                    //MessageBox.Show(j.ToString());
                    this.controles_jugadas.Add(l);
                    this.Controls.Add(l);
                    y += 30;
                }
            }

            bool buscarJugada(enumJuegos juego)
            {
                bool control = false;
                foreach(enumJuegos j in jugadorActual.Jugadas)
                {
                    if(j.ToString() == juego.ToString())
                    {
                        control = true;
                    }
                }

                return control;
            }
            
            if (controles_jugadas.Count() == 0 && control == 2)
            {
                MessageBox.Show("No hay jugadas disponibles, debe anulan una para poder continuar.");
                btnAnularJugada.Visible = true;
                JugadasDisponibles(jugadorActual);
                btnSeleccionarJugada.Visible = false;
            }
        }

        void LimpiarControlJugada()
        {
            foreach (Control c in controles_jugadas)
            {
                c.Dispose();
            }
            controles_jugadas.Clear();
        }

        private void Tablero_TirarDados(DADO dado)
        {
            Dado d = new Dado();
            d.Location = new Point((dado.X * d.Width) + 12, 12);
            //d.Location = new Point((dado.X * d.Width) + 1, (dado.Y * d.Height));
            d.DADO = dado;
            d.DefinirValor();
            dados.Add(dado);
            controlDado.Add(d);
            this.Controls.Add(d);
        }

        void LimpiarTablero(List<Control> listado)
        {
            foreach (Dado d in listado)
            {
                d.Dispose();
            }
        }

        void OrdenarDadosSeleccionados()
        {
            int x = 12, y = 164;
            foreach (Dado c in jugadaActual)
            {
                c.Location = new Point(x, y);
                c.Bloqueado = true;
                x += 63;
            }
        }

        int LimpiarTableroParcial()
        {
            int cantidad = 0;
            List<Control> controlDadoTemp = controlDado.ToList();
            //controlDado.Clear();

            foreach (Dado d in controlDadoTemp)
            {
                if (!d.Seleccionado)
                {
                    d.Dispose();
                    dados.Remove(d.DADO);
                    cantidad += 1;
                }
                else
                {
                    jugadaActual.Add(d);
                }
                controlDado.Remove(d);
            }
            OrdenarDadosSeleccionados();
            if (cantidad == 0 && jugadaActual.Count != 5)
            {
                cantidad = 5;
            }
            return cantidad;
        }
        void CambiarDeJugador()
        {
            if (jugadorActual == j1)
            {
                jugadorActual = j2;
            }
            else
            {
                jugadorActual = j1;
            }

            label1.Text = "Es el turno del jugador: " + jugadorActual.Nombre;
        }

        bool CantidadSeleccionada()
        {
            int seleccionados = (from controlJugada c in controles_jugadas
                                           where c.Seleccionado = true
                                           select c).Count();

            if(seleccionados > 1)
            {
                MessageBox.Show("Solo puede seleccionar una jugada.");
                return false;
            }
            else if(seleccionados == 0)
            {
                MessageBox.Show("Debe seleccionar una jugada para continuar.");
                return false;
            }
            else
            {
                return true;
            }
        }

        public void ConfigurarJugadores(string nombrej1, string nombrej2)
        {
            j1 = JUEGO.CrearJugador(nombrej1);
            j2 = JUEGO.CrearJugador(nombrej2);
            anotador.ConfigurarJugadores(j1, j2);
            jugadorActual = j1;
        }
    }
}
