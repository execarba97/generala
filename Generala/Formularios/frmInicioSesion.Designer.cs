﻿namespace Generala
{
    partial class frmInicioSesion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbIniciarSesion = new System.Windows.Forms.GroupBox();
            this.btnIniciarSesion = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbNuevoUsuario = new System.Windows.Forms.GroupBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.txtapellido = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.txtpass = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbRecuperarPass = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtUsuarioCambioPass = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtNuevaPass = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.gbIniciarSesion.SuspendLayout();
            this.gbNuevoUsuario.SuspendLayout();
            this.gbRecuperarPass.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbIniciarSesion
            // 
            this.gbIniciarSesion.Controls.Add(this.btnIniciarSesion);
            this.gbIniciarSesion.Controls.Add(this.label4);
            this.gbIniciarSesion.Controls.Add(this.label3);
            this.gbIniciarSesion.Controls.Add(this.txtUsuario);
            this.gbIniciarSesion.Controls.Add(this.txtPassword);
            this.gbIniciarSesion.Controls.Add(this.label2);
            this.gbIniciarSesion.Controls.Add(this.label1);
            this.gbIniciarSesion.Location = new System.Drawing.Point(9, 10);
            this.gbIniciarSesion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbIniciarSesion.Name = "gbIniciarSesion";
            this.gbIniciarSesion.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbIniciarSesion.Size = new System.Drawing.Size(271, 121);
            this.gbIniciarSesion.TabIndex = 0;
            this.gbIniciarSesion.TabStop = false;
            this.gbIniciarSesion.Text = "Iniciar sesion";
            // 
            // btnIniciarSesion
            // 
            this.btnIniciarSesion.Location = new System.Drawing.Point(74, 89);
            this.btnIniciarSesion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnIniciarSesion.Name = "btnIniciarSesion";
            this.btnIniciarSesion.Size = new System.Drawing.Size(183, 19);
            this.btnIniciarSesion.TabIndex = 1;
            this.btnIniciarSesion.Text = "Iniciar sesion";
            this.btnIniciarSesion.UseVisualStyleBackColor = true;
            this.btnIniciarSesion.Click += new System.EventHandler(this.btnIniciarSesion_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(151, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Olvide mi contraseña";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "¿Eres nuevo?";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(74, 25);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(184, 20);
            this.txtUsuario.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(74, 48);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(184, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Contraseña";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuario";
            // 
            // gbNuevoUsuario
            // 
            this.gbNuevoUsuario.Controls.Add(this.txtEmail);
            this.gbNuevoUsuario.Controls.Add(this.label10);
            this.gbNuevoUsuario.Controls.Add(this.txtnombre);
            this.gbNuevoUsuario.Controls.Add(this.txtapellido);
            this.gbNuevoUsuario.Controls.Add(this.label5);
            this.gbNuevoUsuario.Controls.Add(this.label6);
            this.gbNuevoUsuario.Controls.Add(this.button1);
            this.gbNuevoUsuario.Controls.Add(this.txtusername);
            this.gbNuevoUsuario.Controls.Add(this.txtpass);
            this.gbNuevoUsuario.Controls.Add(this.label7);
            this.gbNuevoUsuario.Controls.Add(this.label8);
            this.gbNuevoUsuario.Location = new System.Drawing.Point(284, 10);
            this.gbNuevoUsuario.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbNuevoUsuario.Name = "gbNuevoUsuario";
            this.gbNuevoUsuario.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbNuevoUsuario.Size = new System.Drawing.Size(271, 167);
            this.gbNuevoUsuario.TabIndex = 6;
            this.gbNuevoUsuario.TabStop = false;
            this.gbNuevoUsuario.Text = "Crear nuevo usuario";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(74, 117);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(184, 20);
            this.txtEmail.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 117);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Email";
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(74, 72);
            this.txtnombre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(184, 20);
            this.txtnombre.TabIndex = 7;
            // 
            // txtapellido
            // 
            this.txtapellido.Location = new System.Drawing.Point(74, 94);
            this.txtapellido.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(184, 20);
            this.txtapellido.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 94);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Apellido";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 72);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nombre";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(74, 140);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(183, 19);
            this.button1.TabIndex = 1;
            this.button1.Text = "Crear usuario";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtusername
            // 
            this.txtusername.Location = new System.Drawing.Point(74, 25);
            this.txtusername.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(184, 20);
            this.txtusername.TabIndex = 3;
            // 
            // txtpass
            // 
            this.txtpass.Location = new System.Drawing.Point(74, 48);
            this.txtpass.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtpass.Name = "txtpass";
            this.txtpass.Size = new System.Drawing.Size(184, 20);
            this.txtpass.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Contraseña";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 25);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Usuario";
            // 
            // gbRecuperarPass
            // 
            this.gbRecuperarPass.Controls.Add(this.button2);
            this.gbRecuperarPass.Controls.Add(this.txtUsuarioCambioPass);
            this.gbRecuperarPass.Controls.Add(this.txtNuevaPass);
            this.gbRecuperarPass.Controls.Add(this.label12);
            this.gbRecuperarPass.Controls.Add(this.label13);
            this.gbRecuperarPass.Location = new System.Drawing.Point(570, 10);
            this.gbRecuperarPass.Margin = new System.Windows.Forms.Padding(2);
            this.gbRecuperarPass.Name = "gbRecuperarPass";
            this.gbRecuperarPass.Padding = new System.Windows.Forms.Padding(2);
            this.gbRecuperarPass.Size = new System.Drawing.Size(271, 108);
            this.gbRecuperarPass.TabIndex = 6;
            this.gbRecuperarPass.TabStop = false;
            this.gbRecuperarPass.Text = "Recuperar contraseña";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(107, 76);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 19);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cambiar contraseña";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtUsuarioCambioPass
            // 
            this.txtUsuarioCambioPass.Location = new System.Drawing.Point(107, 25);
            this.txtUsuarioCambioPass.Margin = new System.Windows.Forms.Padding(2);
            this.txtUsuarioCambioPass.Name = "txtUsuarioCambioPass";
            this.txtUsuarioCambioPass.Size = new System.Drawing.Size(151, 20);
            this.txtUsuarioCambioPass.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 25);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Usuario";
            // 
            // txtNuevaPass
            // 
            this.txtNuevaPass.Location = new System.Drawing.Point(107, 48);
            this.txtNuevaPass.Margin = new System.Windows.Forms.Padding(2);
            this.txtNuevaPass.Name = "txtNuevaPass";
            this.txtNuevaPass.Size = new System.Drawing.Size(151, 20);
            this.txtNuevaPass.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 48);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Nueva contraseña";
            // 
            // frmInicioSesion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 182);
            this.Controls.Add(this.gbRecuperarPass);
            this.Controls.Add(this.gbNuevoUsuario);
            this.Controls.Add(this.gbIniciarSesion);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmInicioSesion";
            this.Text = "Iniciar sesion";
            this.gbIniciarSesion.ResumeLayout(false);
            this.gbIniciarSesion.PerformLayout();
            this.gbNuevoUsuario.ResumeLayout(false);
            this.gbNuevoUsuario.PerformLayout();
            this.gbRecuperarPass.ResumeLayout(false);
            this.gbRecuperarPass.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbIniciarSesion;
        private System.Windows.Forms.Button btnIniciarSesion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbNuevoUsuario;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.TextBox txtapellido;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.TextBox txtpass;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gbRecuperarPass;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtUsuarioCambioPass;
        private System.Windows.Forms.TextBox txtNuevaPass;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}