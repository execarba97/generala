﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class frmEliminarUsuario : Form
    {
        ADMINISTRADOR administrador;

        public frmEliminarUsuario()
        {
            InitializeComponent();
        }

        private void frmEliminarUsuario_Load(object sender, EventArgs e)
        {
            ListarUsuarios();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (comboUsuarios.SelectedItem != null)
            {
                administrador.BajaUsuario(((USUARIO)comboUsuarios.SelectedValue).Username);
                ListarUsuarios();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un usuario para continuar.");
            }
        }

        public void ObtenerAdmin(ADMINISTRADOR admin)
        {
            administrador = admin;
        }

        void ListarUsuarios()
        {
            comboUsuarios.DataSource = null;
            comboUsuarios.DataSource = ADMINISTRADOR.listarUsuarios(true);
        }
    }
}
