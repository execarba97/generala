﻿namespace Generala
{
    partial class frmAltaAdministrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCrearAdmin = new System.Windows.Forms.Button();
            this.comboUsuarios = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCrearAdmin
            // 
            this.btnCrearAdmin.Location = new System.Drawing.Point(13, 42);
            this.btnCrearAdmin.Name = "btnCrearAdmin";
            this.btnCrearAdmin.Size = new System.Drawing.Size(227, 36);
            this.btnCrearAdmin.TabIndex = 8;
            this.btnCrearAdmin.Text = "Crear administrador";
            this.btnCrearAdmin.UseVisualStyleBackColor = true;
            this.btnCrearAdmin.Click += new System.EventHandler(this.btnCrearAdmin_Click);
            // 
            // comboUsuarios
            // 
            this.comboUsuarios.FormattingEnabled = true;
            this.comboUsuarios.Location = new System.Drawing.Point(80, 12);
            this.comboUsuarios.Name = "comboUsuarios";
            this.comboUsuarios.Size = new System.Drawing.Size(160, 24);
            this.comboUsuarios.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Usuarios";
            // 
            // frmAltaAdministrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCrearAdmin);
            this.Controls.Add(this.comboUsuarios);
            this.Controls.Add(this.label1);
            this.Name = "frmAltaAdministrador";
            this.Text = "frmAltaAdministrador";
            this.Load += new System.EventHandler(this.frmAltaAdministrador_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCrearAdmin;
        private System.Windows.Forms.ComboBox comboUsuarios;
        private System.Windows.Forms.Label label1;
    }
}