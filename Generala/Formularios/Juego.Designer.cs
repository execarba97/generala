﻿namespace Generala
{
    partial class Juego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Juego));
            this.btnTirar = new System.Windows.Forms.Button();
            this.btnSeleccionarJugada = new System.Windows.Forms.Button();
            this.btnAnularJugada = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTirar
            // 
            this.btnTirar.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnTirar.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTirar.Location = new System.Drawing.Point(338, 12);
            this.btnTirar.Name = "btnTirar";
            this.btnTirar.Size = new System.Drawing.Size(75, 23);
            this.btnTirar.TabIndex = 0;
            this.btnTirar.Text = "Tirar";
            this.btnTirar.UseVisualStyleBackColor = false;
            this.btnTirar.Click += new System.EventHandler(this.btnTirar_Click);
            // 
            // btnSeleccionarJugada
            // 
            this.btnSeleccionarJugada.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSeleccionarJugada.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeleccionarJugada.Location = new System.Drawing.Point(338, 42);
            this.btnSeleccionarJugada.Name = "btnSeleccionarJugada";
            this.btnSeleccionarJugada.Size = new System.Drawing.Size(75, 23);
            this.btnSeleccionarJugada.TabIndex = 1;
            this.btnSeleccionarJugada.Text = "Seleccionar";
            this.btnSeleccionarJugada.UseVisualStyleBackColor = false;
            this.btnSeleccionarJugada.Click += new System.EventHandler(this.btnSeleccionarJugada_Click);
            // 
            // btnAnularJugada
            // 
            this.btnAnularJugada.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAnularJugada.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnularJugada.Location = new System.Drawing.Point(338, 71);
            this.btnAnularJugada.Name = "btnAnularJugada";
            this.btnAnularJugada.Size = new System.Drawing.Size(75, 23);
            this.btnAnularJugada.TabIndex = 2;
            this.btnAnularJugada.Text = "Anular";
            this.btnAnularJugada.UseVisualStyleBackColor = false;
            this.btnAnularJugada.Visible = false;
            this.btnAnularJugada.Click += new System.EventHandler(this.btnAnularJugada_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Chartreuse;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(420, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // Juego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(927, 433);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAnularJugada);
            this.Controls.Add(this.btnSeleccionarJugada);
            this.Controls.Add(this.btnTirar);
            this.Name = "Juego";
            this.Text = "Juego";
            this.Load += new System.EventHandler(this.Juego_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTirar;
        private System.Windows.Forms.Button btnSeleccionarJugada;
        private System.Windows.Forms.Button btnAnularJugada;
        private System.Windows.Forms.Label label1;
    }
}