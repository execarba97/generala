﻿namespace Generala
{
    partial class frmDesbloquearUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboUsuariosBloqueados = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuarios bloqueados";
            // 
            // comboUsuariosBloqueados
            // 
            this.comboUsuariosBloqueados.FormattingEnabled = true;
            this.comboUsuariosBloqueados.Location = new System.Drawing.Point(162, 13);
            this.comboUsuariosBloqueados.Name = "comboUsuariosBloqueados";
            this.comboUsuariosBloqueados.Size = new System.Drawing.Size(160, 24);
            this.comboUsuariosBloqueados.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 36);
            this.button1.TabIndex = 2;
            this.button1.Text = "Desbloquear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmDesbloquearUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 192);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboUsuariosBloqueados);
            this.Controls.Add(this.label1);
            this.Name = "frmDesbloquearUsuario";
            this.Text = "frmDesbloquearUsuario";
            this.Load += new System.EventHandler(this.frmDesbloquearUsuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboUsuariosBloqueados;
        private System.Windows.Forms.Button button1;
    }
}