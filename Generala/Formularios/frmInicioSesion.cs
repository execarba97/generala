﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class frmInicioSesion : Form
    {
        public frmInicioSesion()
        {
            InitializeComponent();
            gbNuevoUsuario.Visible = false;
            gbRecuperarPass.Visible = false;
        }

        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            Tuple<bool, string> salida = LOGICA.ADMINISTRADOR.ValidarLogin(txtUsuario.Text, txtPassword.Text);

            if (salida.Item1)
            {
                frmLobby lobby = new frmLobby();
                lobby.MdiParent = this.MdiParent;
                lobby.CapturarUsuario(RecuperarUsuario(txtUsuario.Text));
                lobby.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show(salida.Item2);
            }
        }

        USUARIO RecuperarUsuario(string username)
        {
            USUARIO usr = (from USUARIO u in ADMINISTRADOR.listarUsuarios()
                           where u.Username == username
                           select u).FirstOrDefault();

            return usr;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            gbIniciarSesion.Visible = false;
            gbNuevoUsuario.Visible = true;
            gbNuevoUsuario.Location = new Point(12);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtusername.Text) && VALIDADOR.ValidaText(txtpass.Text) && VALIDADOR.ValidaText(txtnombre.Text) && VALIDADOR.ValidaText(txtapellido.Text) && VALIDADOR.ValidaText(txtEmail.Text))
            {
                Tuple<bool, string> salida = USUARIO.AltaUsuarios(txtusername.Text, txtpass.Text, txtnombre.Text, txtapellido.Text, txtEmail.Text);
                
                if(salida.Item1)
                {
                    MessageBox.Show(salida.Item2);
                    gbIniciarSesion.Visible = true;
                    gbNuevoUsuario.Visible = false;
                }
                else
                {
                    MessageBox.Show(salida.Item2);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (VALIDADOR.ValidaText(txtUsuarioCambioPass.Text) && VALIDADOR.ValidaText(txtNuevaPass.Text))
            {
                Tuple<bool, string> salida = USUARIO.CambiarPassword(txtUsuarioCambioPass.Text, txtNuevaPass.Text);

                if (salida.Item1)
                {
                    MessageBox.Show(salida.Item2);
                    gbIniciarSesion.Visible = true;
                    gbRecuperarPass.Visible = false;
                }
                else
                {
                    MessageBox.Show(salida.Item2);
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            gbIniciarSesion.Visible = false;
            gbRecuperarPass.Visible = true;
            gbRecuperarPass.Location = new Point(12);
        }
    }
}
