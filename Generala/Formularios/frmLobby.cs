﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class frmLobby : Form
    {
        USUARIO usuario;

        public frmLobby()
        {
            InitializeComponent();
        }

        private void btnIniciarPartida_Click(object sender, EventArgs e)
        {
            if(VALIDADOR.ValidaText(txtJugador1.Text) && VALIDADOR.ValidaText(txtJugador2.Text))
            {
                Juego frmJuego = new Juego();
                frmJuego.MdiParent = this.MdiParent;
                frmJuego.ConfigurarJugadores(txtJugador1.Text, txtJugador2.Text);
                frmJuego.Show();
                this.Close();
            }
        }

        public void CapturarUsuario(USUARIO u)
        {
            usuario = u;
            label3.Text = "Bienvenido " + u.Username;
        }

        private void frmLobby_Load(object sender, EventArgs e)
        {
            if (usuario is ADMINISTRADOR)
            {
                gbPanelAdmin.Visible = true;
            }
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            frmInicioSesion ins = new frmInicioSesion();
            ins.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmDesbloquearUsuario fdu = new frmDesbloquearUsuario();
            fdu.ObtenerAdmin((ADMINISTRADOR)usuario);
            fdu.Show();
        }

        private void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            frmEliminarUsuario feu = new frmEliminarUsuario();
            feu.ObtenerAdmin((ADMINISTRADOR)usuario);
            feu.Show();
        }

        private void btnNuevoAdmin_Click(object sender, EventArgs e)
        {
            frmAltaAdministrador faa = new frmAltaAdministrador();
            faa.ObtenerAdmin((ADMINISTRADOR)usuario);
            faa.Show();
        }
    }
}
