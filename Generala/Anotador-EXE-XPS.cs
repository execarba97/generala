﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class Anotador : UserControl
    {
        public List<Puntuador> puntuadores = new List<Puntuador>();

        public Anotador()
        {
            InitializeComponent();
        }

        public void generarContadores(JUGADOR j1, JUGADOR j2)
        {
            generar(j1, 198);
            generar(j2, 292);
        }

        void generar(JUGADOR j, int x)
        {
            int y = 108;
            Puntuador p = new Puntuador(enumJuegos.uno, j);
            organizarPuntuador(p, x, y);

            p = new Puntuador(enumJuegos.dos, j);
            organizarPuntuador(p, x, y+33);

            p = new Puntuador(enumJuegos.tres, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.cuatro, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.cinco, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.seis, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.escalera, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.full, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.poker, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.generala, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.generalaDoble, j);
            organizarPuntuador(p, x, y + 33);

            p = new Puntuador(enumJuegos.total, j);
            organizarPuntuador(p, x, y + 33);
        }

        void organizarPuntuador(Puntuador p, int x, int y)
        {
            p.Location = new Point(x, y);
            this.Controls.Add(p);
            puntuadores.Add(p);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        public void IngresarValor(enumJuegos juego, int puntaje)
        {
            
        }
    }
}
