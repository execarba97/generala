﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generala
{
    class VALIDADOR
    {
        //Genere esta clase porque asi puedo centralizar todas las validaciones en una misma clase.
        public static bool ValidarNumero(string valor)
        {
            int i = 0;
            if (int.TryParse(valor, out i))
            {
                return true;
            }
            else
            {
                MessageBox.Show("Los campos numericos no puede estar en blanco y debe contener solo numeros del tipo entero, sin ningun caracter alfanumerico.", "Error al validar numeros", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool ValidaText(string valor)
        {
            if (!string.IsNullOrWhiteSpace(valor) && !string.IsNullOrWhiteSpace(valor))
            {
                return true;
            }
            else
            {
                MessageBox.Show("Los campos de texto no puede estar en blanco.", "Validador de campo string", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }
}
