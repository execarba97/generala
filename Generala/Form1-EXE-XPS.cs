﻿using LOGICA;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Generala
{
    public partial class Form1 : Form
    {
        TABLERO tablero = new TABLERO();
        List<Control> controlDado = new List<Control>();
        List<Control> jugadaActual = new List<Control>();
        List<DADO> dados = new List<DADO>();
        List<controlJugada> controles_jugadas = new List<controlJugada>();
        Anotador anotador = new Anotador();
        JUGADOR j1 = new JUGADOR();
        JUGADOR j2 = new JUGADOR();

        int control = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void ZonaDeJuego_Load(object sender, EventArgs e)
        {

        }

        private void btnTirar_Click(object sender, EventArgs e)
        {
            int cantidad = 0;
            if(control==2)
            {
                control = 0;
                LimpiarTablero(controlDado);
                LimpiarTablero(jugadaActual);
                controlDado.Clear();
                dados.Clear();
                jugadaActual.Clear();
                cantidad = 5;
            }
            else
            {
                control += 1;
                cantidad = LimpiarTableroParcial();
            }
            LimpiarControlJugada();
            tablero.Tirar(cantidad);
            CalcularJugada();


        }

        void CalcularJugada()
        {
            int x = 450, y = 36;
            foreach(enumJuegos j in tablero.CalcularJugada(dados))
            {
                controlJugada l = new controlJugada();
                l.Location = new Point(x, y);
                l.Nombre = j;
                //MessageBox.Show(j.ToString());
                this.controles_jugadas.Add(l);
                this.Controls.Add(l);
                y += 25;
            }
        }

        void LimpiarControlJugada()
        {
            foreach(Control c in controles_jugadas)
            {
                c.Dispose();
            }
            controles_jugadas.Clear();
        }

        void VerSeleccionados()
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            control += 1;
            tablero.TirarDados += Tablero_TirarDados;
            tablero.Tirar(5);
            CalcularJugada();
            this.Controls.Add(anotador);
            anotador.Location = new Point(624, 36);
            //anotador.IngresarValor(enumJuegos.full, 5);
            anotador.generarContadores(j1, j2);
        }


        private void Tablero_TirarDados(DADO dado)
        {
            Dado d = new Dado();
            d.Location = new Point((dado.X * d.Width)+1, (dado.Y * d.Height));
            d.DADO = dado;
            d.DefinirValor();
            dados.Add(dado);
            controlDado.Add(d);
            this.Controls.Add(d);
        }

        void LimpiarTablero(List<Control> listado)
        {
            foreach(Dado d in listado)
            {
                d.Dispose();
            }
        }

        void OrdenarDadosSeleccionados()
        {
            int x = 12, y = 164;
            foreach (Dado c in jugadaActual)
            {
                c.Location = new Point(x, y);
                c.Bloqueado = true;
                x += 63;
            }
        }

        int LimpiarTableroParcial()
        {
            int cantidad = 0;
            List<Control> controlDadoTemp = controlDado.ToList();
            //controlDado.Clear();

            foreach (Dado d in controlDadoTemp)
            {
                if(!d.Seleccionado)
                {
                    d.Dispose();
                    dados.Remove(d.DADO);
                    cantidad += 1;
                }
                else
                {
                    jugadaActual.Add(d);
                }
                controlDado.Remove(d);
            }
            OrdenarDadosSeleccionados();
            if(cantidad == 0 && jugadaActual.Count != 5)
            {
                cantidad = 5;
            }
            return cantidad;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {

        }
    }
}