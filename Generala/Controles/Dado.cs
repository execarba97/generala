﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class Dado : UserControl
    {
        public Dado()
        {
            InitializeComponent();
        }

        private DADO dad;

        public DADO DADO
        {
            get { return dad; }
            set { dad = value; }
        }

        private bool seleccionado = false;

        public bool Seleccionado
        {
            get { return seleccionado; }
        }

        private bool bloqueado = false;

        public bool Bloqueado
        {
            get { return bloqueado = false; }
            set { bloqueado = value; }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if(!seleccionado && !bloqueado)
            {
                seleccionado = true;
                this.BackgroundImage = null;
                this.BackColor = Color.GreenYellow;
            }
            else if(seleccionado && !bloqueado)
            {
                seleccionado = false;
                this.BackgroundImage = Image.FromFile(".\\Imagenes\\fondo.jpg");
                //this.BackColor = Color.White;
            }
        }

        public void DefinirValor()
        {
            pictureBox1.Image = Image.FromFile(".\\Imagenes\\dado_" + dad.Valor.ToString() + ".png");
        }
    }
}
