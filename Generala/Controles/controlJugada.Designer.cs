﻿namespace Generala
{
    partial class controlJugada
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(controlJugada));
            this.lblJuego = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblJuego
            // 
            this.lblJuego.Location = new System.Drawing.Point(2, 2);
            this.lblJuego.Name = "lblJuego";
            this.lblJuego.Size = new System.Drawing.Size(75, 23);
            this.lblJuego.TabIndex = 0;
            this.lblJuego.Text = "button1";
            this.lblJuego.UseVisualStyleBackColor = true;
            this.lblJuego.Click += new System.EventHandler(this.button1_Click);
            // 
            // controlJugada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.lblJuego);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "controlJugada";
            this.Size = new System.Drawing.Size(80, 26);
            this.Load += new System.EventHandler(this.controlJugada_Load);
            this.Click += new System.EventHandler(this.controlJugada_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button lblJuego;
    }
}
