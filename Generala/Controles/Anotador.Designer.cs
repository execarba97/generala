﻿namespace Generala
{
    partial class Anotador
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Anotador));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.puntuador2_total = new Generala.Puntuador();
            this.puntuador1_total = new Generala.Puntuador();
            this.puntuador2_generaladoble = new Generala.Puntuador();
            this.puntuador1_generaladoble = new Generala.Puntuador();
            this.puntuador2_generala = new Generala.Puntuador();
            this.puntuador1_generala = new Generala.Puntuador();
            this.puntuador2_poker = new Generala.Puntuador();
            this.puntuador1_poker = new Generala.Puntuador();
            this.puntuador2_full = new Generala.Puntuador();
            this.puntuador1_full = new Generala.Puntuador();
            this.puntuador2_escalera = new Generala.Puntuador();
            this.puntuador1_escalera = new Generala.Puntuador();
            this.puntuador2_seis = new Generala.Puntuador();
            this.puntuador1_seis = new Generala.Puntuador();
            this.puntuador2_cinco = new Generala.Puntuador();
            this.puntuador1_cinco = new Generala.Puntuador();
            this.puntuador2_cuatro = new Generala.Puntuador();
            this.puntuador1_cuatro = new Generala.Puntuador();
            this.puntuador2_tres = new Generala.Puntuador();
            this.puntuador1_tres = new Generala.Puntuador();
            this.puntuador2_dos = new Generala.Puntuador();
            this.puntuador1_dos = new Generala.Puntuador();
            this.puntuador2_uno = new Generala.Puntuador();
            this.puntuador1_uno = new Generala.Puntuador();
            this.lblj1 = new System.Windows.Forms.Label();
            this.lblJ2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(309, 450);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // puntuador2_total
            // 
            this.puntuador2_total.Location = new System.Drawing.Point(215, 380);
            this.puntuador2_total.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_total.Name = "puntuador2_total";
            this.puntuador2_total.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_total.TabIndex = 47;
            // 
            // puntuador1_total
            // 
            this.puntuador1_total.Location = new System.Drawing.Point(141, 380);
            this.puntuador1_total.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_total.Name = "puntuador1_total";
            this.puntuador1_total.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_total.TabIndex = 46;
            // 
            // puntuador2_generaladoble
            // 
            this.puntuador2_generaladoble.Location = new System.Drawing.Point(215, 353);
            this.puntuador2_generaladoble.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_generaladoble.Name = "puntuador2_generaladoble";
            this.puntuador2_generaladoble.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_generaladoble.TabIndex = 45;
            // 
            // puntuador1_generaladoble
            // 
            this.puntuador1_generaladoble.Location = new System.Drawing.Point(141, 353);
            this.puntuador1_generaladoble.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_generaladoble.Name = "puntuador1_generaladoble";
            this.puntuador1_generaladoble.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_generaladoble.TabIndex = 44;
            // 
            // puntuador2_generala
            // 
            this.puntuador2_generala.Location = new System.Drawing.Point(215, 326);
            this.puntuador2_generala.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_generala.Name = "puntuador2_generala";
            this.puntuador2_generala.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_generala.TabIndex = 43;
            // 
            // puntuador1_generala
            // 
            this.puntuador1_generala.Location = new System.Drawing.Point(141, 326);
            this.puntuador1_generala.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_generala.Name = "puntuador1_generala";
            this.puntuador1_generala.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_generala.TabIndex = 42;
            // 
            // puntuador2_poker
            // 
            this.puntuador2_poker.Location = new System.Drawing.Point(215, 299);
            this.puntuador2_poker.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_poker.Name = "puntuador2_poker";
            this.puntuador2_poker.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_poker.TabIndex = 41;
            // 
            // puntuador1_poker
            // 
            this.puntuador1_poker.Location = new System.Drawing.Point(141, 299);
            this.puntuador1_poker.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_poker.Name = "puntuador1_poker";
            this.puntuador1_poker.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_poker.TabIndex = 40;
            // 
            // puntuador2_full
            // 
            this.puntuador2_full.Location = new System.Drawing.Point(215, 272);
            this.puntuador2_full.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_full.Name = "puntuador2_full";
            this.puntuador2_full.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_full.TabIndex = 39;
            // 
            // puntuador1_full
            // 
            this.puntuador1_full.Location = new System.Drawing.Point(141, 272);
            this.puntuador1_full.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_full.Name = "puntuador1_full";
            this.puntuador1_full.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_full.TabIndex = 38;
            // 
            // puntuador2_escalera
            // 
            this.puntuador2_escalera.Location = new System.Drawing.Point(215, 245);
            this.puntuador2_escalera.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_escalera.Name = "puntuador2_escalera";
            this.puntuador2_escalera.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_escalera.TabIndex = 37;
            // 
            // puntuador1_escalera
            // 
            this.puntuador1_escalera.Location = new System.Drawing.Point(141, 245);
            this.puntuador1_escalera.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_escalera.Name = "puntuador1_escalera";
            this.puntuador1_escalera.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_escalera.TabIndex = 36;
            // 
            // puntuador2_seis
            // 
            this.puntuador2_seis.Location = new System.Drawing.Point(215, 218);
            this.puntuador2_seis.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_seis.Name = "puntuador2_seis";
            this.puntuador2_seis.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_seis.TabIndex = 35;
            // 
            // puntuador1_seis
            // 
            this.puntuador1_seis.Location = new System.Drawing.Point(141, 218);
            this.puntuador1_seis.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_seis.Name = "puntuador1_seis";
            this.puntuador1_seis.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_seis.TabIndex = 34;
            // 
            // puntuador2_cinco
            // 
            this.puntuador2_cinco.Location = new System.Drawing.Point(215, 191);
            this.puntuador2_cinco.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_cinco.Name = "puntuador2_cinco";
            this.puntuador2_cinco.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_cinco.TabIndex = 33;
            // 
            // puntuador1_cinco
            // 
            this.puntuador1_cinco.Location = new System.Drawing.Point(141, 191);
            this.puntuador1_cinco.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_cinco.Name = "puntuador1_cinco";
            this.puntuador1_cinco.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_cinco.TabIndex = 32;
            // 
            // puntuador2_cuatro
            // 
            this.puntuador2_cuatro.Location = new System.Drawing.Point(215, 164);
            this.puntuador2_cuatro.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_cuatro.Name = "puntuador2_cuatro";
            this.puntuador2_cuatro.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_cuatro.TabIndex = 31;
            // 
            // puntuador1_cuatro
            // 
            this.puntuador1_cuatro.Location = new System.Drawing.Point(141, 164);
            this.puntuador1_cuatro.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_cuatro.Name = "puntuador1_cuatro";
            this.puntuador1_cuatro.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_cuatro.TabIndex = 30;
            // 
            // puntuador2_tres
            // 
            this.puntuador2_tres.Location = new System.Drawing.Point(215, 137);
            this.puntuador2_tres.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_tres.Name = "puntuador2_tres";
            this.puntuador2_tres.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_tres.TabIndex = 29;
            // 
            // puntuador1_tres
            // 
            this.puntuador1_tres.Location = new System.Drawing.Point(141, 137);
            this.puntuador1_tres.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_tres.Name = "puntuador1_tres";
            this.puntuador1_tres.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_tres.TabIndex = 28;
            // 
            // puntuador2_dos
            // 
            this.puntuador2_dos.Location = new System.Drawing.Point(215, 110);
            this.puntuador2_dos.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_dos.Name = "puntuador2_dos";
            this.puntuador2_dos.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_dos.TabIndex = 27;
            // 
            // puntuador1_dos
            // 
            this.puntuador1_dos.Location = new System.Drawing.Point(141, 110);
            this.puntuador1_dos.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_dos.Name = "puntuador1_dos";
            this.puntuador1_dos.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_dos.TabIndex = 26;
            // 
            // puntuador2_uno
            // 
            this.puntuador2_uno.Location = new System.Drawing.Point(215, 83);
            this.puntuador2_uno.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador2_uno.Name = "puntuador2_uno";
            this.puntuador2_uno.Size = new System.Drawing.Size(29, 23);
            this.puntuador2_uno.TabIndex = 25;
            // 
            // puntuador1_uno
            // 
            this.puntuador1_uno.Location = new System.Drawing.Point(141, 83);
            this.puntuador1_uno.Margin = new System.Windows.Forms.Padding(2);
            this.puntuador1_uno.Name = "puntuador1_uno";
            this.puntuador1_uno.Size = new System.Drawing.Size(29, 23);
            this.puntuador1_uno.TabIndex = 24;
            // 
            // lblj1
            // 
            this.lblj1.AutoSize = true;
            this.lblj1.Location = new System.Drawing.Point(125, 60);
            this.lblj1.Name = "lblj1";
            this.lblj1.Size = new System.Drawing.Size(54, 13);
            this.lblj1.TabIndex = 48;
            this.lblj1.Text = "Jugador 1";
            // 
            // lblJ2
            // 
            this.lblJ2.AutoSize = true;
            this.lblJ2.Location = new System.Drawing.Point(196, 60);
            this.lblJ2.Name = "lblJ2";
            this.lblJ2.Size = new System.Drawing.Size(54, 13);
            this.lblJ2.TabIndex = 49;
            this.lblJ2.Text = "Jugador 2";
            // 
            // Anotador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblJ2);
            this.Controls.Add(this.lblj1);
            this.Controls.Add(this.puntuador2_total);
            this.Controls.Add(this.puntuador1_total);
            this.Controls.Add(this.puntuador2_generaladoble);
            this.Controls.Add(this.puntuador1_generaladoble);
            this.Controls.Add(this.puntuador2_generala);
            this.Controls.Add(this.puntuador1_generala);
            this.Controls.Add(this.puntuador2_poker);
            this.Controls.Add(this.puntuador1_poker);
            this.Controls.Add(this.puntuador2_full);
            this.Controls.Add(this.puntuador1_full);
            this.Controls.Add(this.puntuador2_escalera);
            this.Controls.Add(this.puntuador1_escalera);
            this.Controls.Add(this.puntuador2_seis);
            this.Controls.Add(this.puntuador1_seis);
            this.Controls.Add(this.puntuador2_cinco);
            this.Controls.Add(this.puntuador1_cinco);
            this.Controls.Add(this.puntuador2_cuatro);
            this.Controls.Add(this.puntuador1_cuatro);
            this.Controls.Add(this.puntuador2_tres);
            this.Controls.Add(this.puntuador1_tres);
            this.Controls.Add(this.puntuador2_dos);
            this.Controls.Add(this.puntuador1_dos);
            this.Controls.Add(this.puntuador2_uno);
            this.Controls.Add(this.puntuador1_uno);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Anotador";
            this.Size = new System.Drawing.Size(286, 429);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Puntuador puntuador1_uno;
        private Puntuador puntuador2_uno;
        private Puntuador puntuador2_dos;
        private Puntuador puntuador1_dos;
        private Puntuador puntuador2_tres;
        private Puntuador puntuador1_tres;
        private Puntuador puntuador2_seis;
        private Puntuador puntuador1_seis;
        private Puntuador puntuador2_cinco;
        private Puntuador puntuador1_cinco;
        private Puntuador puntuador2_cuatro;
        private Puntuador puntuador1_cuatro;
        private Puntuador puntuador2_poker;
        private Puntuador puntuador1_poker;
        private Puntuador puntuador2_full;
        private Puntuador puntuador1_full;
        private Puntuador puntuador2_escalera;
        private Puntuador puntuador1_escalera;
        private Puntuador puntuador2_total;
        private Puntuador puntuador1_total;
        private Puntuador puntuador2_generaladoble;
        private Puntuador puntuador1_generaladoble;
        private Puntuador puntuador2_generala;
        private Puntuador puntuador1_generala;
        private System.Windows.Forms.Label lblj1;
        private System.Windows.Forms.Label lblJ2;
    }
}
