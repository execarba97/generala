﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class Anotador : UserControl
    {
        public List<Puntuador> puntuadores = new List<Puntuador>();

        public Anotador()
        {
            InitializeComponent();
        }

        public void configurarContadores(JUGADOR j1, JUGADOR j2)
        {
            puntuador1_uno.configurar(enumJuegos.uno, j1);
            puntuador1_dos.configurar(enumJuegos.dos, j1);
            puntuador1_tres.configurar(enumJuegos.tres, j1);
            puntuador1_cuatro.configurar(enumJuegos.cuatro, j1);
            puntuador1_cinco.configurar(enumJuegos.cinco, j1);
            puntuador1_seis.configurar(enumJuegos.seis, j1);
            puntuador1_escalera.configurar(enumJuegos.escalera, j1);
            puntuador1_full.configurar(enumJuegos.full, j1);
            puntuador1_poker.configurar(enumJuegos.poker, j1);
            puntuador1_generala.configurar(enumJuegos.generala, j1);
            puntuador1_generaladoble.configurar(enumJuegos.generalaDoble, j1);
            puntuador1_total.configurar(enumJuegos.total, j1);

            puntuador2_uno.configurar(enumJuegos.uno, j2);
            puntuador2_dos.configurar(enumJuegos.dos, j2);
            puntuador2_tres.configurar(enumJuegos.tres, j2);
            puntuador2_cuatro.configurar(enumJuegos.cuatro, j2);
            puntuador2_cinco.configurar(enumJuegos.cinco, j2);
            puntuador2_seis.configurar(enumJuegos.seis, j2);
            puntuador2_escalera.configurar(enumJuegos.escalera, j2);
            puntuador2_full.configurar(enumJuegos.full, j2);
            puntuador2_poker.configurar(enumJuegos.poker, j2);
            puntuador2_generala.configurar(enumJuegos.generala, j2);
            puntuador2_generaladoble.configurar(enumJuegos.generalaDoble, j2);
            puntuador2_total.configurar(enumJuegos.total, j2);

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        public void IngresarValor(enumJuegos juego, int puntaje, JUGADOR jugador)
        {
            foreach(Control c in this.Controls)
            {
                if(c is Puntuador)
                {
                    if(((Puntuador)c).jugador == jugador && ((Puntuador)c).juego == juego)
                    {
                        ((Puntuador)c).Anotar(puntaje);
                    }
                }
            }
        }

        public void ConfigurarJugadores(JUGADOR jugador1, JUGADOR jugador2)
        {
            lblj1.Text = jugador1.Nombre;
            lblJ2.Text = jugador2.Nombre;
        }
    }
}
