﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class controlJugada : UserControl
    {
        public controlJugada()
        {
            InitializeComponent();
        }

        private void controlJugada_Load(object sender, EventArgs e)
        {

        }

        private enumJuegos nombre;

        public enumJuegos Nombre
        {
            get { return nombre; }
            set { 
                nombre = value;
                lblJuego.Text = nombre.ToString();
            }
        }

        private bool seleccionado;

        public bool Seleccionado
        {
            get { return seleccionado; }
            set { seleccionado = value; }
        }

        private void lblJuego_Click_1(object sender, EventArgs e)
        {
            Seleccionar();
        }

        private void controlJugada_Click(object sender, EventArgs e)
        {
            Seleccionar();
        }

        void Seleccionar()
        {
            if (!seleccionado)
            {
                seleccionado = true;
                this.BackgroundImage = null;
                this.BackColor = Color.GreenYellow;
            }
            else if (seleccionado)
            {
                seleccionado = false;
                this.BackgroundImage = Image.FromFile(".\\Imagenes\\base.jpg");
                this.BackColor = Color.White;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Seleccionar();
        }
    }
}
