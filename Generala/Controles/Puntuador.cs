﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LOGICA;

namespace Generala
{
    public partial class Puntuador : UserControl
    {
        public Puntuador()
        {
            InitializeComponent();
        }

        public void configurar(enumJuegos enu, JUGADOR j)
        {
            juego = enu;
            jugador = j;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void Anotar(int puntos = 0)
        {
            if (puntos == 0)
            {
                label1.Text = "anulado";
            }
            else
            {
                label1.Text = puntos.ToString();
            }
        }

        public enumJuegos juego;
        public JUGADOR jugador;

    }
}
