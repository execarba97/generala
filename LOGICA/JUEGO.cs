﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LOGICA
{
    public class JUEGO
    {
        public JUEGO(enumJuegos tip, int puntos)
        {
            tipo = tip;
            puntaje = puntos;

        }

        private enumJuegos tipo;

        public enumJuegos Tipo
        {
            get { return tipo; }
        }

        private int puntaje;

        public int Puntaje
        {
            get { return puntaje; }
        }

        public static bool Generala(List<DADO> Dados)
        {
            bool Generala = false;
            int contador = 1;

            for (int i = 1; i < Dados.Count; i++)
            {
                if (((DADO)Dados[i]).Valor == ((DADO)Dados[0]).Valor)
                {
                    contador++;
                }
            }

            if (contador == 5)
            {
                Generala = true;
            }

            return Generala;
        }

        public static bool Poker(List<DADO> Dados)
        {
            int numero1 = 0, numero2 = 0, numero3 = 0, numero4 = 0, numero5 = 0, numero6 = 0;
            foreach (DADO d in Dados)
            {
                if (d.Valor == 1)
                {
                    numero1++;
                }
                if (d.Valor == 2)
                {
                    numero2++;
                }
                if (d.Valor == 3)
                {
                    numero3++;
                }
                if (d.Valor == 4)
                {
                    numero4++;
                }
                if (d.Valor == 5)
                {
                    numero5++;
                }
                if (d.Valor == 6)
                {
                    numero6++;
                }
            }

            if(numero1 == 4 || numero2 == 4 || numero3 == 4 || numero4 == 4 || numero5 == 4 || numero6 == 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Escalera(List<DADO> Dados)
        {
            List<DADO> dadosOrquenados = Dados.OrderBy(o => o.Valor).ToList();

            if (dadosOrquenados[0].Valor == 1 && dadosOrquenados[1].Valor == 2 && dadosOrquenados[2].Valor == 3 && dadosOrquenados[3].Valor == 4 && dadosOrquenados[4].Valor == 5)
            {
                return true;
            }

            else if (dadosOrquenados[0].Valor == 2 && dadosOrquenados[1].Valor == 3 && dadosOrquenados[2].Valor == 4 && dadosOrquenados[3].Valor == 5 && dadosOrquenados[4].Valor == 6)
            {
                return true;
            }

            else if (dadosOrquenados[0].Valor == 3 && dadosOrquenados[1].Valor == 4 && dadosOrquenados[2].Valor == 5 && dadosOrquenados[3].Valor == 6 && dadosOrquenados[4].Valor == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Full(List<DADO> Dados)
        {
            List<DADO> dadosOrquenados = Dados.OrderBy(o => o.Valor).ToList();
            int val1 = dadosOrquenados[0].Valor, val2 = dadosOrquenados[4].Valor, acumulador1 = 0, acumulador2 = 0;

            foreach (DADO d in dadosOrquenados)
            {
                if(d.Valor == val1)
                {
                    acumulador1 += 1;
                }
                else if(d.Valor == val2)
                {
                    acumulador2 += 1;
                }
            }

            if((acumulador1 == 2 && acumulador2 == 3) || (acumulador1 == 3 && acumulador2 == 2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static List<enumJuegos> Numero(List<DADO> Dados)
        {
            List<enumJuegos> numeros = new List<enumJuegos>();
            int flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0, flag6 = 0;

            foreach(DADO d in Dados)
            {
                if(d.Valor == 1 && flag1 == 0)
                {
                    numeros.Add(enumJuegos.uno);
                    flag1 = 1;
                }
                else if(d.Valor == 2 && flag2 == 0)
                {
                    numeros.Add(enumJuegos.dos);
                    flag2 = 1;
                }
                else if (d.Valor == 3 && flag3 == 0)
                {
                    numeros.Add(enumJuegos.tres);
                    flag3 = 1;
                }
                else if (d.Valor == 4 && flag4 == 0)
                {
                    numeros.Add(enumJuegos.cuatro);
                    flag4 = 1;
                }
                else if (d.Valor == 5 && flag5 == 0)
                {
                    numeros.Add(enumJuegos.cinco);
                    flag5 = 1;
                }
                else if (d.Valor == 6 && flag6 == 0)
                {
                    numeros.Add(enumJuegos.seis);
                    flag6 = 1;
                }
            }

            return numeros;
        }

        public static int CalcularNumero(List<DADO> Dados, int numero)
        {
            int puntaje = 0;
            
            foreach(DADO d in Dados)
            {
                if(d.Valor == numero)
                {
                    puntaje += d.Valor;
                }
            }

            return puntaje;
        }

        public static int CalcularPuntos(enumJuegos juego, int tiradas, List<DADO> Dados)
        {
            if(juego == enumJuegos.uno)
            {
                return CalcularNumero(Dados, 1);
            }
            if (juego == enumJuegos.dos)
            {
                return CalcularNumero(Dados, 2);
            }
            if (juego == enumJuegos.tres)
            {
                return CalcularNumero(Dados, 3);
            }
            if (juego == enumJuegos.cuatro)
            {
                return CalcularNumero(Dados, 4);
            }
            if (juego == enumJuegos.cinco)
            {
                return CalcularNumero(Dados, 5);
            }
            if (juego == enumJuegos.seis)
            {
                return CalcularNumero(Dados, 6);
            }

            if (juego == enumJuegos.escalera && tiradas == 0)
            {
                return 25;
            }
            else if(juego == enumJuegos.escalera)
            {
                return 20;
            }

            if (juego == enumJuegos.poker && tiradas == 0)
            {
                return 45;
            }
            else if (juego == enumJuegos.poker)
            {
                return 40;
            }

            if (juego == enumJuegos.full && tiradas == 0)
            {
                return 35;
            }
            else if (juego == enumJuegos.full)
            {
                return 30;
            }

            if (juego == enumJuegos.generala && tiradas == 0)
            {
                return 60;
            }
            else if (juego == enumJuegos.generala)
            {
                return 10000;
            }

            if (juego == enumJuegos.generalaDoble && tiradas == 0)
            {
                return 100;
            }
            else if (juego == enumJuegos.generalaDoble)
            {
                return 10000;
            }

            return 0;
        }

        public static JUGADOR CrearJugador(string nombre)
        {
            JUGADOR j = new JUGADOR();
            j.Nombre = nombre;
            JUGADOR.AltaJugador(j);

            return j;
        }
    }
}