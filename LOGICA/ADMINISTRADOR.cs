﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acceso;
using System.Data;
using System.Data.SqlClient;
namespace LOGICA
{
    public class ADMINISTRADOR : USUARIO
    {
        public ADMINISTRADOR(int id, string usrname, string pass, string usrnombre, string usrapellido, int usrintentos, int block, string usrmail, int usrestado):base(id, usrname, pass, usrnombre, usrapellido, usrintentos, block, usrmail, usrestado)
        {

        }

        public static List<USUARIO> listarUsuarios(bool bloqueados = false, bool admin = false)
        {
            List<USUARIO> listaDeUsuarios = new List<USUARIO>();
            ACCESO acceso = new ACCESO();
            DataTable tabla;

            acceso.Abrir();

            if(bloqueados)
            {
                tabla = acceso.Leer("ObtenerUsuariosBloqueados", CommandType.StoredProcedure);
            }
            if(admin)
            {
                tabla = acceso.Leer("ObtenerNoAdministradores", CommandType.StoredProcedure);
            }
            else
            {
                tabla = acceso.Leer("ObtenerUsuarios", CommandType.StoredProcedure);
            }

            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                USUARIO usr;

                if (int.Parse(registro["administrador"].ToString()) == 1)
                {
                    usr = new ADMINISTRADOR(int.Parse(registro["userID"].ToString()), registro["username"].ToString(), registro["password"].ToString(), registro["nombre"].ToString(), registro["apellido"].ToString(), int.Parse(registro["intentos"].ToString()), int.Parse(registro["bloqueado"].ToString()), registro["email"].ToString(), int.Parse(registro["estado"].ToString()));
                }
                else
                {
                    usr = new USUARIO(int.Parse(registro["userID"].ToString()), registro["username"].ToString(), registro["password"].ToString(), registro["nombre"].ToString(), registro["apellido"].ToString(), int.Parse(registro["intentos"].ToString()), int.Parse(registro["bloqueado"].ToString()), registro["email"].ToString(), int.Parse(registro["estado"].ToString()));
                }

                listaDeUsuarios.Add(usr);
            }

            return listaDeUsuarios;
        }

        public Tuple<bool, string> DesbloquearUsuario(string username)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado = "";
            bool estado = false;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("username", username));

            DataTable tabla = acceso.Leer("DesbloquearUsuario", CommandType.StoredProcedure, paratros);

            foreach (DataRow registro in tabla.Rows)
            {
                resultado = registro["Resultado"].ToString();
            }

            acceso.Cerrar();

            if(resultado == "1")
            {
                resultado = "Usuario desbloqueado";
                estado = true;
            }
            else if(resultado == "-1")
            {
                resultado = "Usuario no encontrado";
            }

            return new Tuple<bool, string>(estado, resultado);
        }

        public static Tuple<bool, string> ValidarLogin(string username, string password)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado = "";
            bool estado = false;

            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CreaParametro("username", username));
            parametros.Add(acceso.CreaParametro("password", password));

            DataTable tabla = acceso.Leer("ValidaLogin", CommandType.StoredProcedure, parametros);

            foreach (DataRow registro in tabla.Rows)
            {
                resultado = registro["Resultado"].ToString();
            }

            if (resultado == "1")
            {
                resultado = "Inicio correcto";
                estado = true;
            }
            else if (resultado == "0")
            {
                resultado = "Usuario no encontrado";
            }
            else if (resultado == "-2")
            {
                resultado = "Usuaurio bloqueado";
            }
            else if (resultado == "-3")
            {
                resultado = "Usuaurio bloqueado";
            }
            else if (resultado == "-4")
            {
                resultado = "Contraseña inconrrecta";
            }

            return new Tuple<bool, string>(estado, resultado);
        }

        public string BajaUsuario(string username)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("username", username));

            resultado = acceso.Escribir("BajaUsuario", paratros);
            acceso.Cerrar();

            return resultado;
        }

        public string AltaAdministrador(string username)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("username", username));

            resultado = acceso.Escribir("AltaAdmin", paratros);
            acceso.Cerrar();

            return resultado;
        }
    }
}