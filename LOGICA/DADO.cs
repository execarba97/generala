﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LOGICA
{
    public class DADO
    {
        static Random rnd = new Random();
        public DADO(int coordenadaX, int coordenadaY)
        {
            x = coordenadaX;
            y = coordenadaY;
            valor = rnd.Next(1, 7);
        }

        private int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        private int valor;

        public int Valor
        {
            get { return valor; }
        }

    }
}