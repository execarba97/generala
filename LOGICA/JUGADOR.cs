﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acceso;
using System.Data;
using System.Data.SqlClient;

namespace LOGICA
{
    public class JUGADOR
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int puntos = 0;

        public int Puntos
        {
            get { return puntos; }
        }

        private List<enumJuegos> jugadas = new List<enumJuegos>();

        public List<enumJuegos> Jugadas
        {
            get { return jugadas; }
            set { jugadas = value; }
        }

        public void AnotarJugada(enumJuegos j, int p)
        {
            puntos += p;
            jugadas.Add(j);
        }

        public static void AltaJugador(JUGADOR jugador)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("nombre", jugador.Nombre));

            resultado = acceso.Escribir("AltaJugador", paratros);
            acceso.Cerrar();
        }
    }
}