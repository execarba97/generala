﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LOGICA
{
    public enum enumJuegos
    {
        uno,
        dos,
        tres,
        cuatro,
        cinco,
        seis,
        escalera,
        full,
        generala,
        generalaDoble,
        poker,
        total
    }
}