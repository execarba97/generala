﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acceso;
using System.Data;
using System.Data.SqlClient;
namespace LOGICA
{
    public class USUARIO
    {
        public USUARIO(int id, string usrname, string pass, string usrnombre, string usrapellido, int usrintentos, int block, string usrmail, int usrestado)
        {
            userID = id;
            username = usrname;
            password = pass;
            nombre = usrnombre;
            apellido = usrapellido;
            bloqueado = block;
            intentos = usrintentos;
            email = usrmail;
            estado = usrestado;
        }
        private int userID;

        public int UserID
        {
            get { return userID; }
        }

        private string username;

        public string Username
        {
            get { return username; }
        }

        private string password;

        public string Password
        {
            get { return password; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
        }

        private int bloqueado;

        public int Bloqueado
        {
            get { return bloqueado; }
        }

        private int intentos;

        public int Intentos
        {
            get { return intentos; }
        }

        private string email;

        public string Email
        {
            get { return email; }
        }

        private int estado;

        public int Estado
        {
            get { return estado; }
        }

        public static Tuple<bool, String> AltaUsuarios(string username, string password, string nombre, string apellido, string email)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado = "";
            bool estado = false;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("nombre", nombre));
            paratros.Add(acceso.CreaParametro("apellido", apellido));
            paratros.Add(acceso.CreaParametro("username", username));
            paratros.Add(acceso.CreaParametro("password", password));
            paratros.Add(acceso.CreaParametro("administrador", int.Parse("0")));
            paratros.Add(acceso.CreaParametro("email", email));

            DataTable tabla = acceso.Leer("AltaUsuario", CommandType.StoredProcedure ,paratros);

            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                resultado = registro["Resultado"].ToString();
            }

            if (resultado == "1")
            {
                resultado = "Usuario dado de alta correctamente";
                estado = true;
            }
            else if (resultado == "0")
            {
                resultado = "El usuario ya existe";
            }

            return new Tuple<bool, string>(estado, resultado);
        }

        public static Tuple<bool, String> CambiarPassword(string username, string password)
        {
            ACCESO acceso = new ACCESO();
            acceso.Abrir();
            string resultado = "";
            bool estado = false;

            List<SqlParameter> paratros = new List<SqlParameter>();
            paratros.Add(acceso.CreaParametro("username", username));
            paratros.Add(acceso.CreaParametro("password", password));

            DataTable tabla = acceso.Leer("CambiarPass", CommandType.StoredProcedure, paratros);

            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                resultado = registro["Resultado"].ToString();
            }

            if (resultado == "1")
            {
                resultado = "Contraseña cambiada correctamente.";
                estado = true;
            }
            else if (resultado == "-1")
            {
                resultado = "El usuario es invalido.";
            }
            else
            {
                resultado = "Error en la ejecucion del SP";
            }

            return new Tuple<bool, string>(estado, resultado);
        }

        public override string ToString()
        {
            return username;
        }
    }
}