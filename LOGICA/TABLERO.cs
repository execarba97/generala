﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class TABLERO
    {
        public delegate void delTirarDados(DADO dado);
        public event delTirarDados TirarDados;

        public static Random rnd = new Random();
        List<DADO> dados = new List<DADO>();
        
        public List<DADO> Dados
        {
            get { return dados; }
        }

        //public void Tirar()
        //{
        //    for(int fila = 0; fila <3; fila++)
        //    {
        //        for (int columna = 0; columna < 2; columna++)
        //        {
        //            DADO dado = new DADO(fila, columna);
        //            this.TirarDados(dado);

        //            dados.Add(dado);
        //        }
        //    }
        //}

        public void Tirar(int cantidad)
        {
            for (int columna = 0; columna < cantidad; columna++)
            {
                DADO dado = new DADO(columna, 0);
                this.TirarDados(dado);

                dados.Add(dado);
            }
        }

        public List<enumJuegos> CalcularJugada(List<DADO> dadosObtenidos)
        {
            List<enumJuegos> juegos = new List<enumJuegos>();

            if(JUEGO.Escalera(dadosObtenidos))
            {
                juegos.Add(enumJuegos.escalera);
            }
            
            if(JUEGO.Full(dadosObtenidos))
            {
                juegos.Add(enumJuegos.full);
            }

            if (JUEGO.Generala(dadosObtenidos))
            {
                juegos.Add(enumJuegos.generala);
            }

            if (JUEGO.Poker(dadosObtenidos))
            {
                juegos.Add(enumJuegos.poker);
            }

            foreach(enumJuegos e in JUEGO.Numero(dadosObtenidos))
            {
                juegos.Add(e);
            }

            return juegos;
        }
    }
}
