USE [master]
GO
/****** Object:  Database [GENERALA]    Script Date: 11/11/2020 2:14:47 AM ******/
CREATE DATABASE [GENERALA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GENERALA', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\GENERALA.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'GENERALA_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\GENERALA_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [GENERALA] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GENERALA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GENERALA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GENERALA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GENERALA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GENERALA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GENERALA] SET ARITHABORT OFF 
GO
ALTER DATABASE [GENERALA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GENERALA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GENERALA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GENERALA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GENERALA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GENERALA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GENERALA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GENERALA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GENERALA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GENERALA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GENERALA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GENERALA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GENERALA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GENERALA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GENERALA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GENERALA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GENERALA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GENERALA] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GENERALA] SET  MULTI_USER 
GO
ALTER DATABASE [GENERALA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GENERALA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GENERALA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GENERALA] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [GENERALA] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [GENERALA] SET QUERY_STORE = OFF
GO
USE [GENERALA]
GO
/****** Object:  Table [dbo].[jugadas]    Script Date: 11/11/2020 2:14:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[jugadas](
	[id] [int] NULL,
	[nombre] [varchar](50) NULL,
	[puntos] [int] NULL,
	[idJugador] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[jugadores]    Script Date: 11/11/2020 2:14:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[jugadores](
	[id] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[puntos] [int] NULL,
 CONSTRAINT [PK_jugadores] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[userID] [int] NOT NULL,
	[username] [varchar](100) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[apellido] [varchar](100) NOT NULL,
	[intentos] [int] NOT NULL,
	[bloqueado] [int] NOT NULL,
	[administrador] [int] NOT NULL,
	[email] [varchar](150) NOT NULL,
	[estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[AltaAdmin]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaAdmin]
@username varchar(100)
as
begin
	UPDATE [dbo].[usuarios]
	set administrador = 1
	where username = @username
end
GO
/****** Object:  StoredProcedure [dbo].[AltaJugador]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AltaJugador]
@nombre varchar(50)
as
begin
	declare @id int
	set @id = (SELECT ISNULL(MAX(id), 0)+1 FROM dbo.jugadores)
	INSERT INTO [dbo].[jugadores]
           ([id],[nombre],[puntos])
    VALUES (@id,@nombre,0)
end
GO
/****** Object:  StoredProcedure [dbo].[AltaUsuario]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AltaUsuario]
@username varchar(100),
@password varchar(100),
@nombre varchar(100),
@apellido varchar(100),
@administrador int,
@email varchar(150)
as
begin
	declare @id int, @estado int, @bloqueado int, @intentos int

	if(select count(username) from usuarios where username = @username) = 0
		begin
			set @bloqueado = 0
			set @estado = 1
			set @intentos = 0
			set @id = (SELECT ISNULL(MAX(userID), 0)+1 FROM dbo.usuarios)
			INSERT INTO [dbo].[usuarios] ([userID],[username],[password],[nombre],[apellido],[intentos],[bloqueado],[administrador],[email],[estado])
			VALUES
				   (@id
				   ,@username
				   ,@password
				   ,@nombre
				   ,@apellido
				   ,0
				   ,@bloqueado
				   ,@administrador
				   ,@email
				   ,@estado);

			select '1' as Resultado;
		end
	 else
		begin
			select '0' as Resultado;
		end
end
GO
/****** Object:  StoredProcedure [dbo].[BajaAdmin]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaAdmin]
@username varchar(100)
as
begin
	UPDATE [dbo].[usuarios]
	set administrador = 0
	where username = @username
end
GO
/****** Object:  StoredProcedure [dbo].[BajaUsuario]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[BajaUsuario]
@username varchar(100)
as
begin
	UPDATE [dbo].[usuarios]
	set estado = 0
	where username = @username
end
GO
/****** Object:  StoredProcedure [dbo].[CambiarPass]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CambiarPass]
@username varchar(100),
@password varchar(100)
as
begin
	if(select count(username) from [dbo].[usuarios] where username = @username) = 1
		begin
			UPDATE [dbo].[usuarios] SET bloqueado = 0, intentos = 0, password = @password where username = @username;
			select '1' as Resultado;
		end
	else
		select '-1' as Resultado
end
GO
/****** Object:  StoredProcedure [dbo].[DesbloquearUsuario]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[DesbloquearUsuario]
@username varchar(100)
as
begin
	if(select count(username) from [dbo].[usuarios] where username = @username) = 1
		begin
			UPDATE [dbo].[usuarios] SET bloqueado = 0, intentos = 0 where username = @username;
			return 1;
		end
	else
		return -1
end
GO
/****** Object:  StoredProcedure [dbo].[ObtenerNoAdministradores]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ObtenerNoAdministradores]
as
begin
	SELECT [userID]
      ,[username]
      ,[password]
      ,[nombre]
      ,[apellido]
      ,[intentos]
      ,[bloqueado]
      ,[administrador]
      ,[email]
      ,[estado]
  FROM [dbo].[usuarios]
  where estado = 1 and administrador = 0
end
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuarios]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ObtenerUsuarios]
as
begin
	SELECT [userID]
      ,[username]
      ,[password]
      ,[nombre]
      ,[apellido]
      ,[intentos]
      ,[bloqueado]
      ,[administrador]
      ,[email]
      ,[estado]
  FROM [dbo].[usuarios]
  where estado = 1
end
GO
/****** Object:  StoredProcedure [dbo].[ObtenerUsuariosBloqueados]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[ObtenerUsuariosBloqueados]
as
begin
	SELECT [userID]
      ,[username]
      ,[password]
      ,[nombre]
      ,[apellido]
      ,[intentos]
      ,[bloqueado]
      ,[administrador]
      ,[email]
      ,[estado]
  FROM [dbo].[usuarios]
  where bloqueado = 1 and estado = 1
end
GO
/****** Object:  StoredProcedure [dbo].[ValidaLogin]    Script Date: 11/11/2020 2:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ValidaLogin]

@username varchar(100),
@password varchar(100)
as
begin
	declare @passwordAux varchar(100)

	if (select count(username) from [dbo].[usuarios] where username = @username) = 1
		begin
			set @passwordAux = (select password from [dbo].[usuarios] where username = @username)

			if(select password from [dbo].[usuarios] where username = @username and bloqueado = 0) = @password
				begin
					UPDATE [dbo].[usuarios] SET intentos = 0 where username = @username;
					select 1 as 'Resultado';
				end
			else if(select bloqueado from [dbo].[usuarios] where username = @username) = 1
				begin
					select -2 as 'Resultado';
				end
			else
				begin
					declare @intentos int
					set @intentos = (SELECT (intentos)+1 FROM [dbo].[usuarios] where username = @username)
					UPDATE [dbo].[usuarios] SET intentos = @intentos where username = @username;
					if (select intentos from [dbo].[usuarios] where username = @username) = 3
						begin
							UPDATE [dbo].[usuarios] SET bloqueado = 1 where username = @username;
							select -3 as 'Resultado';
						end
					else
						select -4 as 'Resultado';
				end
		end
	else
		select 0 as 'Resultado';

end;
GO
USE [master]
GO
ALTER DATABASE [GENERALA] SET  READ_WRITE 
GO
